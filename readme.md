# Nikolaos Pothitos

DevOps Engineer · [Vivalerts](https://vivalerts.com) founder · PhD

👇 Hit _read more_ below for the full CV

![Nikolaos Pothitos](https://gitlab.com/pothitos/pothitos/-/raw/main/Gallery/Nikos.jpg)

🌐 [gitlab.com/pothitos](https://gitlab.com/pothitos)  
🌐 [github.com/pothitos](https://github.com/pothitos)

## Employment

👷 __2014–Today__ _DevOps Engineer_ at _Nokia_

Continuous Integration and Testing expert acquainted with
Kanban and Agile methodologies for the following areas

  + The 5G _Short Message Service Function_ (SMSF)
  + The _Nokia Telecom Application Server_ containerized
    product for voice, messaging, and video sessions
  + The _Inner Source_ components for multiple products
  + The _Virtual Gateway Platform_ product which deploys 4G
    telecommunication applications in cloud infrastructures

C++ trainer registered in LAEK (Special Fund for Employment
and Vocational Training)

👷 __2012–2014__ _Computerization_, database design, reports
generation for the magazines redistribution company _Lobby_

👷 __2009–2011__ _Professor_ in _Vocational Training
Institutes_ for the courses: Programming, Algorithms & Data
Structures, Software Engineering, Unix Operating Systems,
Communication & Internet Technologies, Introduction to
Computing, and Graduation Projects

👷 __2008__ _Software Engineer_ for embedded systems in the
IT company _Infinite Data Technology_

👷 __2008__ _Analyst/Programmer_ at _Hellenic Army Staff_;
military service fulfilled

👷 __2003__ _English Professor_ at _Evangelopoulos_ tutorial
school

## Informatics Specialties

![Skills](https://gitlab.com/pothitos/pothitos/-/raw/main/Skills.png)

## Education

🎓 __2022__ Ph.D. with honors at the National and
Kapodistrian University of Athens (NKUA) in the Department
of Informatics and Telecommunications. Dissertation title:
[Constraint Programming: Algorithms and
Systems](https://pothitos.gitlab.io/pothitos/thesis.pdf)

💰 Partially funded by _C++ libraries for constraint
programming_ NKUA and _Handling uncertainty in data
intensive applications on a distributed computing
environment_ Thales projects

🎓 __2009__ M.Sc. with honors at NKUA in the Department of
Informatics and Telecommunications. M.Sc. Title: _Advanced
Information Systems_

🎓 __2005__ B.Sc. at NKUA in the Department of Informatics
and Telecommunications. Direction: _Computer Systems and
Applications_

## Awards and Scholarship

🏆 __2022__ Nokia Athens R&D Innovation award

🏆 __2022__ [Vivalerts](https://vivalerts.com) was one of
the seven finalists of the 1<sup>st</sup> NKUA Innovation
and Entrepreneurship competition

🏆 __2011__ Bodossaki Foundation scholarship for Ph.D.
studies

🏆 __2000__ Mayor of Athens medal for high school
performance

## Publications

📜
Nikolaos Pothitos and Panagiotis Stamatopoulos.
The dilemma between arc and bounds consistency.
_International Journal of Intelligent Systems_,
  35(10):1467–1491, 2020 ·
[PDF](https://pothitos.gitlab.io/pothitos/Pothitos2020.pdf) ·
[Publisher](https://doi.org/10.1002/int.22259) ·
[BibTeX](https://gitlab.com/pothitos/pothitos/-/blob/main/Pothitos2020.bib)

📜
Nikolaos Pothitos and Panagiotis Stamatopoulos.
Building search methods with self-confidence in a
constraint programming library.
_International Journal on Artificial Intelligence Tools_,
  27(4):1860003, 2018 ·
[PDF](https://pothitos.gitlab.io/pothitos/Pothitos2018.pdf) ·
[Publisher](https://doi.org/10.1142/S0218213018600035) ·
[BibTeX](https://gitlab.com/pothitos/pothitos/-/blob/main/Pothitos2018.bib)

📜
Nikolaos Pothitos and Panagiotis Stamatopoulos.
Piece of Pie Search: Confidently exploiting heuristics.
In _SETN 2016_, pages 8:1–8:8, New York, 2016. ACM ·
[PDF](https://pothitos.gitlab.io/pothitos/Pothitos2016-PoPS.html) ·
[BibTeX](https://gitlab.com/pothitos/pothitos/-/blob/main/Pothitos2016-PoPS.bib)

📜
Nikolaos Pothitos and Panagiotis Stamatopoulos.
Constraint Programming MapReduce'd.
In _SETN 2016_, pages 5:1–5:4, New York, 2016. ACM ·
[PDF](https://pothitos.gitlab.io/pothitos/Pothitos2016-CPMR.html) ·
[BibTeX](https://gitlab.com/pothitos/pothitos/-/blob/main/Pothitos2016-CPMR.bib)

📜
Nikolaos Pothitos, Panagiotis Stamatopoulos, and Kyriakos Zervoudakis.
Course scheduling in an adjustable constraint propagation schema.
In _ICTAI 2012_, volume 1, pages 335–343. IEEE, 2012 ·
[PDF](https://pothitos.gitlab.io/pothitos/Pothitos2012-Scheduling.pdf) ·
[Publisher](https://doi.org/10.1109/ICTAI.2012.53) ·
[BibTeX](https://gitlab.com/pothitos/pothitos/-/blob/main/Pothitos2012-Scheduling.bib)

📜
Nikolaos Pothitos, George Kastrinis, and Panagiotis Stamatopoulos.
Constraint propagation as the core of local search.
In _SETN 2012_, volume 7297 of _LNCS (LNAI)_, pages 9–16,
  Heidelberg, 2012. Springer ·
[PDF](https://pothitos.gitlab.io/pothitos/Pothitos2012-CBLS.pdf) ·
[Publisher](https://doi.org/10.1007/978-3-642-30448-4_2) ·
[BibTeX](https://gitlab.com/pothitos/pothitos/-/blob/main/Pothitos2012-CBLS.bib)

📜
Nikolaos Pothitos and Panagiotis Stamatopoulos.
Flexible management of large-scale integer domains in CSPs.
In _SETN 2010_, volume 6040 of _LNCS (LNAI)_, pages 405–410,
  Heidelberg, 2010. Springer ·
[PDF](https://pothitos.gitlab.io/pothitos/Pothitos2010-Domains.pdf) ·
[Publisher](https://doi.org/10.1007/978-3-642-12842-4_51) ·
[BibTeX](https://gitlab.com/pothitos/pothitos/-/blob/main/Pothitos2010-Domains.bib)

📜
Nikolaos Pothitos.
Constraint propagation and search methods in constraint programming.
In _Selected Graduate and Undergraduate Theses_, volume 7, pages
  31–40. Department of Informatics and Telecommunications, NKUA, 2010 ·
[PDF in Greek](https://pothitos.gitlab.io/pothitos/Pothitos2010-Propagation.pdf)

📜
Nikolaos Pothitos.
Automated timetable construction with constraint programming.
In _Selected Graduate and Undergraduate Theses_, volume 4, pages
  169–178. Department of Informatics and Telecommunications, NKUA, 2007 ·
[PDF in Greek](https://pothitos.gitlab.io/pothitos/Pothitos2007.pdf)

## Languages

🌐 __English__ University of Cambridge: Certificate of
Proficiency, 1997

🌐 __French__ D.E.L.F. 1<sup>er</sup> Degré, 1998

## Teaching Assistant in NKUA

👓 _Introduction to Programming_ academic years '05-'06,
'06-'07, '11-'12, '12-'13

👓 _Object-Oriented Programming_ academic years '05-'06,
'06-'07, '08-'09, '09-'10, '10-'11, '11-'12, '12-'13,
'13-'14

👓 _Logic Programming_ academic years '08-'09, '09-'10,
'10-'11, '11-'12, '12-'13, '13-'14

👓 _Supervisor_ of diploma theses, three of which were
selected among the best ones in their respective years

## Scientific Activities

🖋️ _Program Committee_ member in the _36<sup>th</sup> IEEE
International Conference on Tools with Artificial
Intelligence_ (ICTAI 2024) and _Session Chair_ for two
sessions in ICTAI 2012

🖋️ _Reviewer_ for the _International Journal of Intelligent
Systems_ and the conferences CIAC 2025, SETN 2024, 2010,
2006, ICTAI 2018, 2017, 2007, CIMA 2018, SEA 2017, ESA 2016

🖋️ _Byzantine Music Student_ at the Athens Archdiocese
School, 2012–2015

## Knowledge and Skills Test Score: 95%  

Organized by the Greek public hiring sector (ASEP)

## Hobbies ⚽ 🏊 🚴

<details>
  <summary><h2>Photo Gallery 📸</h2></summary>

  ### 1987: Preschool photo
  ![](https://gitlab.com/pothitos/pothitos/-/raw/main/Gallery/1987-Preschool.jpg)

  ### 2000: Leaving the mayoral awards ceremony at Megaron
  ![](https://gitlab.com/pothitos/pothitos/-/raw/main/Gallery/2000-Megaron.jpg)

  ### 2012: Presenting at SETN conference at Lamia
  ![](https://gitlab.com/pothitos/pothitos/-/raw/main/Gallery/2012-SETN-Lamia.jpg)

  ### 2016: Presenting at SETN conference at Salonica
  ![](https://gitlab.com/pothitos/pothitos/-/raw/main/Gallery/2016-SETN-Salonica-1.jpg)

  ![](https://gitlab.com/pothitos/pothitos/-/raw/main/Gallery/2016-SETN-Salonica-2.jpg)

  ### 2023: First [Vivalerts](https://vivalerts.com) (formerly Socileaks) public presentation
  ![](https://gitlab.com/pothitos/pothitos/-/raw/main/Gallery/2023-Socileaks.jpg)

  ### 2023: With Prof. Panagiotis Stamatopoulos at the PhD graduation ceremony
  ![](https://gitlab.com/pothitos/pothitos/-/raw/main/Gallery/2023-Graduation.jpg)

  ### 2023: I was awarded a €300 gift card as Nokia Athens Innovation Award
  ![](https://gitlab.com/pothitos/pothitos/-/raw/main/Gallery/2023-Nokia-Innovation-Award.jpg)
</details>

<details>
  <summary><h2>My Greek Links 🇬🇷</h2></summary>

   - Ph.D. Thesis [Προγραμματισμός με Περιορισμούς: Αλγόριθμοι και Συστήματα](https://pothitos.gitlab.io/pothitos/thesis.pdf)
   - M.Sc. Thesis [Μέθοδοι διάδοσης περιορισμών και αναζήτησης στον Προγραμματισμό με Περιορισμούς](https://pothitos.gitlab.io/pothitos/MScThesis.pdf)
   - B.Sc. Thesis [Αυτόματη κατασκευή ωρολογίων προγραμμάτων μέσω Προγραμματισμού με Περιορισμούς](https://pothitos.gitlab.io/pothitos/BScThesis.pdf)
   - Παρασκευή Ποθητού [Οικοδόμηση σπιτιών στην ορεινή Νάξο κατά τα παλαιά χρόνια 1900–1950](https://pothitos.gitlab.io/pothitos/Oikodomisi-Orini-Naxos-1900-1950.pdf)
   - [Πέντε συμβουλές για μία παρουσίαση με διαφάνειες](https://cgi.di.uoa.gr/~pothitos/el/tips.html)
   - [Five tips for a slides presentation](https://cgi.di.uoa.gr/~pothitos/tips.html)
   - [Επεκτατός κατακερματισμός](https://cgi.di.uoa.gr/~pothitos/xhash)

  ### Κείμενα

   - [Ανθρωποδείκτες](https://gist.github.com/pothitos/9da725d8867aaabfaa7f5d23a1404916)
   - [Πολιτική πυξίδα](https://gist.github.com/pothitos/5b139341b19f61a0307073fe0abeba74)
   - [Νέοι και εκκλησία](http://antiairetikos.blogspot.gr/2012/02/blog-post_26.html)
   - [Εκκλησία και εμβολιασμός](https://cgi.di.uoa.gr/~pothitos/COVID-19)
   - [Τεχνητή νοημοσύνη και ελευθερία βούλησης](http://tinyurl.com/aiwill)
   - [Γέροντας Παΐσιος: Ακρογωνιαίος λίθος και πέτρα σκανδάλου](http://megalomartys.wordpress.com/2013/12/17/%CE%B3%CE%AD%CF%81%CE%BF%CE%BD%CF%84%CE%B1%CF%82-%CF%80%CE%B1%CE%90%CF%83%CE%B9%CE%BF%CF%82-%E1%BC%80%CE%BA%CF%81%CE%BF%CE%B3%CF%89%CE%BD%CE%B9%CE%B1%E1%BF%96%CE%BF%CF%82-%CE%BB%CE%AF%CE%B8%CE%BF/)
   - [Σταχυολόγηση στίχων από το Κατά Ματθαίον Ευαγγέλιο που μου φάνηκαν αισιόδοξοι](https://cgi.di.uoa.gr/~pothitos/el/Matthew.html)
</details>
